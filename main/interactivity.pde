int selected_species = 0;
int reaction_selector = 0;
Map<String, Integer> participants = new HashMap<String, Integer>();
Map<Integer, String> participants_r = new HashMap<Integer, String>();
PVector selector = new PVector(0, 0, 0);

List<Interaction> interactions = new ArrayList<Interaction>();

public interface Interaction {
  
  int species();
  
  void do_();
}

//puts reactant into the volume
void introduceStuff1() {
  float[] normSelector = normalizeSelector(selector);
  Cell cc = rg.world[int(normSelector[0])][int(normSelector[1])][int(normSelector[2])];
  cc.concentration_v[selected_species] += 1.0;
}

//puts product into the volume
void introduceStuff2() {
  float[] normSelector = normalizeSelector(selector);
  Cell cc = rg.world[int(normSelector[0])][int(normSelector[1])][int(normSelector[2])];
  cc.concentration_v[selected_species] += 1.0;
}

float[] normalizeSelector(PVector selector) {
  float[] normSelector = selector.array();
  normSelector[0] = map(normSelector[0], -world_dim*delta/2, world_dim*delta/2, 0, world_dim - 1);
  normSelector[0] = constrain(normSelector[0], 0, world_dim);
  normSelector[1] = world_dim - map(normSelector[1], -world_dim*delta/2, world_dim*delta/2, 0, world_dim) - 1;
  normSelector[1] = constrain(normSelector[1], 0, world_dim);
  normSelector[2] = world_dim - map(normSelector[2], -world_dim*delta/2, world_dim*delta/2, 0, world_dim) - 1;
  normSelector[2] = constrain(normSelector[2], 0, world_dim);
  return normSelector;
}

void draw_selector() {
  pushMatrix();
  pushStyle();
  translate(selector.x, selector.y + 25, selector.z + 30);
  fill(color(0.1, 0.3, 0.9, 0.5));
  box(world_dim, world_dim, world_dim);
  popStyle();
  popMatrix();
}
