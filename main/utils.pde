/** 
 * Utility functions
 */

int color_mult(color a, float b) {
   return color_mult(a, color(b, b, b, b));
 }
 
 color[] globalPalette = producePalette(10);
 
 /**
  * Element-wise color addition
  */
 int color_add(color... cs) {
   float ar = 0.0f;
   float ag = 0.0f;
   float ab = 0.0f;
   float aa = 0.0f;
   for(int i = 0; i < cs.length; i++) {
     ar += red(cs[i]);
     ag += green(cs[i]);
     ab += blue(cs[i]);
     aa += alpha(cs[i]);
   }
   return color(ar, ag, ab, aa);
 }
 
 
  /* Product a color palette of n colors */
 color[] producePalette(int n) {
   color[] palette = new color[n];
   palette[0] = #F3F315;
   palette[1] = #83F52C;
   palette[2] = #FF6600;
   palette[3] = #FF0099;
   palette[4] = #6E0DD0;
   for(int i = 5; i < n; i++) {
     palette[i] = color(random(0.0,1.0),random(0.0,1.0),random(0.0,1.0), 1);
   }
   return palette;
 }
 
 void pretty_display_reaction(Reaction r, float x, float y) {
   StringBuilder sb = new StringBuilder();
 }
 
 /**
  * Element-wise color multiplication
  */
 int color_mult(color a, color b) {
   float ar = red(a);
   float ag = green(a);
   float ab = blue(a);
   float aa = alpha(a);
   
   float br = red(b);
   float bg = green(b);
   float bb = blue(b);
   float ba = alpha(b);
   return color(ar*br, ag*bg, ab*bb, aa*ba);
 }
 
 
 
 /**
  * Change c's alpha to newAlpha
  */
 color modAlpha(color c, float newAlpha) {
   return color(red(c), green(c), blue(c), newAlpha);
 }

/**
 * This mod respects your integers
 */ 
int mod(int a, int b) {
  return ((a % b) + b) % b;
}

float distance(int X, int Y, int Z) {
  return sqrt(X*X + Y*Y + Z*Z);
}

float[] concentration_interpolation(float X, float Y, float Z, int s, int numSpecies) {
  int x0 = floor(X);
  int y0 = floor(Y);
  int z0 = floor(Z);
  
  int xLow = ((x0 - s + 1) >= 0) ? x0 - s + 1 : 0;
  int yLow = ((y0 - s + 1) >= 0) ? y0 - s + 1 : 0;
  int zLow = ((z0 - s + 1) >= 0) ? z0 - s + 1 : 0;
    
  int xHi = ((x0 + s) <= world_dim) ? x0 + s : world_dim;
  int yHi = ((y0 + s) <= world_dim) ? y0 + s : world_dim;
  int zHi = ((z0 + s) <= world_dim) ? z0 + s : world_dim;
  
  float[] concentration = new float[numSpecies];
  float totalDistance = 0.0;
  float distance;
  
  for(int species = 0; species < numSpecies; species++) {
    for(int i = xLow; i < xHi; i++) {
      for(int j = yLow; j < yHi; j++) {
        for(int k = zLow; k < zHi; k++) {
          distance = distance(i, j, k);
          totalDistance += distance;
          concentration[species] += rg.world[i][j][k].concentration_v[species] / distance;
        }
      }
    }
  }
  
  for(int species = 0; species < numSpecies; species++) {
    concentration[species] /= totalDistance;
  }
  
  return concentration;
}

float[] add(float[] a, float[] b) {
  if(a.length != b.length) throw new IllegalArgumentException("Vector length must match.");
  float[] result = new float[a.length];
  for(int i = 0; i < a.length; i++) {
    result[i] = a[i] + b[i];
  }
  return result;
} 
