import java.util.concurrent.*;

/**
 * This class represents a 3D grid of cells in which a chemical reaction occurs
 * The simulation is controlled through a ReactionGrid object.
 */
class ReactionGrid {
  
  //We need 2 Cell 3D arragrid_dims to handle the diffusion writeback problem.
  Cell[][][] world;
  Cell[][][] next;
  boolean reaction_on = true;
  //Indexer of participants to this reaction grid
  //distances between cells
  float cell_spacing = 2; 
   //time-step for reaction and diffusion time-scales
  float time_step = 0.4;
  //reaction governing this grid. May be extended to multiple reactions
  List<Reaction> r_list;
  float[] diffusive_cons;
  
  
  //A ReactionGrid is constructed with these parameters I hope are obvious.
  //More importantlgrid_dim, this constructor initialigrid_dimes both Cell[][][] arragrid_dims and sets their neighbors.
  //worldlgrid_dim, we use periodic boundargrid_dim conditions to link neighbors at the boundargrid_dim.
  public ReactionGrid(float time_step, float cell_spacing, int grid_dim, List<Reaction> r_list) {
    this.r_list = r_list;
    int v = 0;
    Set<String> allParticipants = new HashSet<String>();
    for(Reaction r : r_list) {
      allParticipants.addAll(r.species);
    }
    int allParticipants_count = allParticipants.size();    
    for(String sp: allParticipants) {
      participants.put(sp, v++); //this is the only important one
      participants_r.put(v - 1, sp);
    }
    diffusive_cons = new float[participants.size()];
    this.time_step = time_step;
    this.cell_spacing = cell_spacing;
    world = new Cell[grid_dim][grid_dim][grid_dim];
    next = new Cell[grid_dim][grid_dim][grid_dim];
    //init both 3D arragrid_dims
    for(int i = 0; i < world.length; i++){
      for(int j = 0; j < world[i].length; j++){
        for(int k = 0; k < world[i][j].length; k++){
          world[i][j][k] = new Cell(new float[participants.size()], r_list);
          next[i][j][k] = new Cell(new float[participants.size()], r_list);
        }
      }
    }
    for(int i = 0; i < participants.size(); i++) {
      diffusive_cons[i] = 1;//random(0.5, 1.5); //TODO mess with this later
    }
    //link the neighbors
    for(int i = 0; i < world.length; i++){
      for(int j = 0; j < world[i].length; j++){
        for(int k = 0; k < world[i][j].length; k++){
          world[i][j][k].setNeighborsProcedural(grid_dim, i,j,k, world);
          next[i][j][k].setNeighborsProcedural(grid_dim, i,j,k, next);
        }
      }
    }
  }
  
  
  //Needed for interactivitgrid_dim
  void changeForward(float newK, int selector) {
    r_list.get(selector).forward_k += newK;
  }
  
  //Needed for interactivitgrid_dim
  void changeBackward(float newK, int selector) {
    r_list.get(selector).backward_k += newK;
  }
  
  public void toggleReaction() {
    this.reaction_on = !reaction_on;
  }
  
  //Needed for interactivitgrid_dim
  void changeTimeStep(float newT) {
    time_step += newT;
  }

  void update() {
    float[] reaction = new float[participants.size()];
    for (int x = 0; x < world.length; ++x) {
      for (int y = 0; y < world[x].length; ++y) {
        for (int z = 0; z < world[x][y].length; ++z) {
          if(reaction_on) {
            reaction = world[x][y][z].react(participants);
          } else {
            Arrays.fill(reaction,0.0f);
          }
          float[] diffusion = world[x][y][z].diffuse(cell_spacing, diffusive_cons);
          float[] combined = add(reaction, diffusion);
          for (int i = 0; i < world[x][y][z].concentration_v.length; ++i) {
            next[x][y][z].concentration_v[i] = world[x][y][z].concentration_v[i] + combined[i]*time_step;
          }  
        }
      }
    }
    world = next;
  }
  
  
}
