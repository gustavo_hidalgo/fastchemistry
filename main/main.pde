import peasy.*;
import java.util.List;
import java.util.*;


PeasyCam camera;
int world_dim = 20;
ReactionGrid rg;
float k_f, k_b, t;
int i_i, i_j, i_k;
List<Reaction> list;
float time = 0.0;
PImage[] levels = new PImage[world_dim];
//you'll need to install PeasyCam
//http://mrfeinberg.com/peasycam/#download
void setup() {
  //Here we init the ReactionGrid and the PImages used for texture mapping.
  //Ignore unless you want to know more...
  camera = new PeasyCam(this, 0, 0, 0, world_dim*70);
  camera.setSuppressRollRotationMode();
  colorMode(RGB, 1.0, 1.0, 1.0, 1.0);
  size(600, 600, P3D);
  reset();
}

void reset() {
  
  k_f = 1.0;
  k_b = 1.0;
  t = 0.25;
  ReactionBuilder rb1 = new ReactionBuilder();
  ReactionBuilder rb2 = new ReactionBuilder();
  rb1
    .reactants(2)
    .products(1)
    .reactantOrders(new float[] {1.0, 1.0})
    .productOrders(new float[] {1.0})
    .reactantCoefficients(new float[] {1.0,1.0})
    .productCoefficients(new float[] {1.0})
    .forwardK(k_f)
    .backwardK(k_b)
    .setNames(Arrays.asList("A", "B", "C"));
    
  rb2
    .reactants(1)
    .products(2)
    .reactantOrders(new float[] {1.0})
    .productOrders(new float[] {1.0,1.0})
    .reactantCoefficients(new float[] {2.0})
    .productCoefficients(new float[] {1.0, 1.0})
    .forwardK(k_f)
    .backwardK(k_b)
    .setNames(Arrays.asList("C", "A", "B"));
    
  list = new ArrayList<Reaction>();
  list.add(rb1.build());
  list.add(rb2.build());
  rg = new ReactionGrid(t, 2.0, world_dim, list);
  //new TwoOne_Reaction(k_f, k_b, Arrays.asList("a")));
  //create levels for texture mapping
  for (int i = 0; i < world_dim; i++) {
    levels[i] = createImage(world_dim, world_dim, ARGB);
  }
  try {
    output = new File("out.csv");
    writer = new PrintStream(output);
    
  } catch (Exception e) {
    e.printStackTrace();
  }
  writer.print("t");
  for(String s : participants.keySet()) {
    writer.print(","+s);
  }
  writer.println();
}

//inter-plane distance. Probably don't mess with this.... 30 seems to work.
int delta = 30;

//continous presses
void keys() {
  if (key == 'p') {
    introduceStuff2();
  }
  if (key == 'w') {
    selector.y -= delta;
    if (selector.y < -delta*world_dim/2) {
      selector.y = -delta*world_dim/2;
    }
  }
  if (key == 'a') {
    selector.x -= delta;
    if (selector.x < -delta*world_dim/2) {
      selector.x = -delta*world_dim/2;
    }
  }
  if (key == 's') {
    selector.y += delta;
    if (selector.y > delta*world_dim/2) {
      selector.y = delta*world_dim/2;
    }
  }
  if (key == 'd') {
    selector.x += delta;
    if (selector.x > delta*world_dim/2) {
      selector.x = delta*world_dim/2;
    }
  }
  if (key == 'q') {
    selector.z += delta;
    if (selector.z > delta*world_dim/2) {
      selector.z = delta*world_dim/2;
    }
  }
  if (key == 'e') {
    selector.z -= delta;
    if (selector.z < -delta*world_dim/2) {
      selector.z = -delta*world_dim/2;
    }
  }
}

//single presses
void keyPressed() {
  if (key == 'f') {
    k_f += 0.1;
    rg.changeForward(0.1, reaction_selector);
  }
  if (key == 'r') {
    rg.toggleReaction();
  }
  if (key == 'F') {
    k_f -= 0.1;
    rg.changeForward(-0.1, reaction_selector);
  }
  if (key == 'b') {
    k_b += 0.1;
    rg.changeBackward(0.1, reaction_selector);
  }
  if (key == 'B') {
    rg.changeBackward(-0.1, reaction_selector);
    k_b -= 0.1;
  }
  if (key == 't') {
    t += 0.05;
    rg.changeTimeStep(0.05);
  }
  if (key == 'T') {
    t -= 0.05;
    rg.changeTimeStep(-0.05);
  }
  if (key == 'i') {
    float[] normSelector = normalizeSelector(selector);
    i_i = int(normSelector[0]);
    i_j = int(normSelector[1]);
    i_k = int(normSelector[2]);
    instrument = !instrument;
  }
  if(key == 'z') {
    float[] normSelector = normalizeSelector(selector);
    final Cell cc = rg.world[int(normSelector[0])][int(normSelector[1])][int(normSelector[2])];
    interactions.add(new Interaction() {
      private int s = selected_species;
      public void do_() {
        cc.concentration_v[species()] += 0.05;
      }
      public int species() {
        return s;
      }
    });
  }
  if(key == 'Z') {
    interactions.clear();
  }
  if(key >= '0' && key <= '9' && Integer.parseInt(""+key) < participants.size()) {
    selected_species = Integer.parseInt(""+key);
  }
  if(key == '!') {
    reaction_selector = 0;
  }
  if(key == '@' && list.size() > 1) {
    reaction_selector = 1;
  }
  if(key == '#' && list.size() > 2) {
    reaction_selector = 2;
  }
  if(key == '$' && list.size() > 3) {
    reaction_selector = 3;
  }
}

void display_information() {
  camera.beginHUD();
  StringBuilder sb = new StringBuilder();
  textSize(10);
  String warning = "";
  if (t > 0.5) {
    warning = "Careful!";
  } else {
    warning = "";
  }
  fill(0, 0, 0, 1);
  sb.append("Selected reaction: " + reaction_selector).append("  Shift+[number] to select").append("\n");
  sb.append("Forward K (f/F): " + String.format("%3.3f", rg.r_list.get(reaction_selector).forward_k)).append("\n");
  sb.append(("Backward K (b/B): " + String.format("%3.3f", rg.r_list.get(reaction_selector).backward_k))).append("\n");
  sb.append("Time step (t/T): " + String.format("%3.3f", t)).append(warning).append("\n");
  sb.append("p : spawn product. r : toggle reaction").append("\n");
  float[] v =  rg.world[i_i][i_j][i_k].concentration_v;
  sb.append(String.format("i : Instrument (%d, %d, %d): ", i_i, i_j, i_k));
  sb.append("[ ");
  for(float f : rg.world[i_i][i_j][i_k].concentration_v) {
    sb.append(String.format("%2.3f, ", f));
  }
  sb.append("]\n");
  for(int r = 0; r < list.size(); r++) {
    sb.append("Reaction "+r+")" + String.format(" %s", list.get(r).toString())).append("\n");
  }
  sb.append("Selected species: " + String.format("%s", participants_r.get(selected_species))).append(" [number] to select").append("\n");
  sb.append("WASDQE to navigate selector cube").append("\n");
  text(sb.toString(), 20,20);
  camera.endHUD();
}

void writeLine() {
  writer.print(time);
  for(String s : participants.keySet()){
    writer.print(","+rg.world[i_i][i_j][i_k].concentration_v[participants.get(s)]);
  }
  writer.println();
}

//main drawing loop
void draw() {
  background(0.5);
  if (keyPressed) keys();
  draw_selector();
  rg.update();
  time += t;
  writeLine();
  if (key == 'R') {
    reset(); //in-case of breaking bugs, might happen on long time_steps
    return;
  } else {
    if(instrument) {
    }
    display_information();
    for(Interaction i : interactions) {
      i.do_();
    }
    for (int i = 0; i < world_dim; i++) {
      drawQuad(i); //scary scary function
    }
    noFill();
    stroke(0);
    box(world_dim*delta, world_dim*delta, world_dim*delta);
  }
}

//DO NOT TOUCH! very scary
//This draws the 3D world by detecting where the User's camera is,
//Writing the ReactionGrid color state to some textures in the correct orientation
//Drawing a series of quads in back to front order with the textures mapped correctly.
//If you see anything funky with the rendering, tell me and I'll examine this for correctness.
void drawQuad(int i) {
  noStroke(); //If you wanna see how it works, comment this line out!
  float[] rots = camera.getRotations();
  float[] pos = camera.getPosition();
  PVector cam_vector = new PVector(pos[0], pos[1], pos[2]);
  cam_vector.normalize();
  if (-sqrt(2)/2 < cam_vector.y && cam_vector.y < sqrt(2)/2) {
    //if on the main viewing plane
    if (cam_vector.z > 0 && -sqrt(2)/2 < cam_vector.x && cam_vector.x < sqrt(2)/2) {
      //front
      PImage img = levels[i];
      img.loadPixels();
      for (int x = 0; x < rg.world[i].length; x++) {
        for (int y = 0; y < rg.world[i][x].length; y++) {
          img.pixels[y*img.height + x] = rg.world[x][y][world_dim - i - 1].getColor();
        }
      }
      img.updatePixels();
      beginShape(QUADS);
      texture(img);
      //negative Z towards positive z
      vertex(-15*world_dim, -15*world_dim, -delta/2*(world_dim-1) + i*delta, 0, img.height);
      vertex(15*world_dim, -15*world_dim, -delta/2*(world_dim-1) + i*delta, img.width, img.height);
      vertex(15*world_dim, 15*world_dim, -delta/2*(world_dim-1) + i*delta, img.width, 0         );
      vertex(-15*world_dim, 15*world_dim, -delta/2*(world_dim-1) + i*delta, 0, 0);
      endShape();
    } else if (cam_vector.z < 0 && cam_vector.x > -sqrt(2)/2 && cam_vector.x < sqrt(2)/2) {
      PImage img = levels[i];
      img.loadPixels();
      for (int x = 0; x < rg.world[i].length; x++) {
        for (int y = 0; y < rg.world[i][x].length; y++) {
          img.pixels[y*img.height + x] = rg.world[x][y][i].getColor();
        }
      }
      img.updatePixels();
      beginShape(QUADS);
      texture(img);
      //positive Z towards negative z
      vertex(-15*world_dim, -15*world_dim, delta/2*(world_dim-1) - i*delta, 0, img.height);
      vertex(15*world_dim, -15*world_dim, delta/2*(world_dim-1) - i*delta, img.width, img.height);
      vertex(15*world_dim, 15*world_dim, delta/2*(world_dim-1) - i*delta, img.width, 0         );
      vertex(-15*world_dim, 15*world_dim, delta/2*(world_dim-1) - i*delta, 0, 0);
      endShape();
    } else if (cam_vector.x < 0 && cam_vector.z < sqrt(2)/2 && cam_vector.z > -sqrt(2)/2) {
      PImage img = levels[i];
      img.loadPixels();
      for (int y = 0; y < rg.world[i].length; y++) {
        for (int z = 0; z < rg.world[i][y].length; z++) {
          img.pixels[y*img.height + z] = rg.world[world_dim -i -1][y][world_dim - z -1].getColor();
        }
      }
      img.updatePixels();
      beginShape(QUADS);
      texture(img);
      //positive x towards negative x
      vertex(delta/2*(world_dim-1) - i*delta, -15*world_dim, -15*world_dim, 0, img.height);
      vertex(delta/2*(world_dim-1) - i*delta, 15*world_dim, -15*world_dim, 0, 0);
      vertex(delta/2*(world_dim-1) - i*delta, 15*world_dim, 15*world_dim, img.width, 0);
      vertex(delta/2*(world_dim-1) - i*delta, -15*world_dim, 15*world_dim, img.width, img.height);
      endShape();
    } else {
      PImage img = levels[i];
      img.loadPixels();
      for (int y = 0; y < rg.world[i].length; y++) {
        for (int z = 0; z < rg.world[i][y].length; z++) {
          img.pixels[y*img.height + z] = rg.world[i][y][world_dim - z -1].getColor();
        }
      }
      img.updatePixels();
      beginShape(QUADS);
      texture(img);
      //negative x towards positive x
      vertex(-(world_dim-1)/2*delta + i*delta, -15*world_dim, -15*world_dim, 0, img.height);
      vertex(-(world_dim-1)/2*delta + i*delta, 15*world_dim, -15*world_dim, 0, 0);
      vertex(-(world_dim-1)/2*delta + i*delta, 15*world_dim, 15*world_dim, img.width, 0);
      vertex(-(world_dim-1)/2*delta + i*delta, -15*world_dim, 15*world_dim, img.width, img.height);
      endShape();
    }
  } else if (cam_vector.y > 0) {
    //looking from above
    PImage img = levels[i];
    img.loadPixels();
    for (int x = 0; x < rg.world[i].length; x++) {
      for (int z = 0; z < rg.world[i][x].length; z++) {
        img.pixels[z*img.height + x] = rg.world[x][world_dim - i - 1][z].getColor();
      }
    }
    img.updatePixels();
    beginShape(QUADS);
    texture(img);
    //negative y towards positive y
    vertex(-15*world_dim, -delta/2*(world_dim-1) + i*delta, -15*world_dim, 0, img.height);
    vertex(15*world_dim, -delta/2*(world_dim-1) + i*delta, -15*world_dim, img.width, img.height);
    vertex(15*world_dim, -delta/2*(world_dim-1) + i*delta, 15*world_dim, img.width, 0         );
    vertex(-15*world_dim, -delta/2*(world_dim-1) + i*delta, 15*world_dim, 0, 0);
    endShape();
  } else {
    PImage img = levels[i];
    img.loadPixels();
    for (int x = 0; x < rg.world[i].length; x++) {
      for (int z = 0; z < rg.world[i][x].length; z++) {
        img.pixels[z*img.height + x] = rg.world[x][i][world_dim - z - 1].getColor();
      }
    }
    img.updatePixels();
    beginShape(QUADS);
    texture(img);
    //pos y towards neg y
    vertex(-15*world_dim, delta/2*(world_dim-1) - i*delta, -15*world_dim, 0, 0);
    vertex(15*world_dim, delta/2*(world_dim-1) - i*delta, -15*world_dim, img.width, 0);
    vertex(15*world_dim, delta/2*(world_dim-1) - i*delta, 15*world_dim, img.width, img.height);
    vertex(-15*world_dim, delta/2*(world_dim-1) - i*delta, 15*world_dim, 0, img.height);
    endShape();
  }
}

