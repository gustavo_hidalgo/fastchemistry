/**
 * A reaction object contains the chemical kinetic information for a particular reaction.
 * This is an abstract class because the number of reactants and products is left to the subclass to define.
 *
 */
abstract class Reaction {
  
  protected float forward_k;
  protected float backward_k;
  protected float[] coefficients;
  protected float[] orders;
  public final List<String> species;
  
  //A reaction is constructed with both the forward and backward chemical reaction rate constants,
  //stochiometric coefficient arrays of reactants and products, and partial order constants.
  //NOTE: the length of the coefficient arrays must match the length of the order arrays for 
  //reactants and products respectively.
  public Reaction(float forward_k, float backward_k, float[] coefficients, float[] orders, List<String> species) {
    this.forward_k = forward_k;
    this.backward_k = backward_k;
    this.coefficients = coefficients;
    this.orders = orders;
    this.species = species;
  }
  
  //Forwark k modifier for interactivity purposes.
  void changeForward(float newK) {
    this.forward_k += newK;
  }
  
  //Backward k modifier for interactivity purposes.
  void changeBackward(float newK) {
    this.backward_k += newK;
  }
  
  //all tells this reaction what indices in "state" correspond to the
  //species that it manages.
  float[] react(float[] state, Map<String, Integer> all) {
    float normalized_rate = 0.0f;
    float react_product = 1.0;
    float product_product = 1.0;
    /*Product of all reactant concetrations raised to their respective orders*/
    for(int i = 0; i < reactantCount(); i++) {
      String reactant = species.get(i);
      int state_index = all.get(reactant);
      react_product *= Math.pow(state[state_index], orders[i]);
    }
    /*Product of all product concentrations raised to their respective orders*/
    for(int i = reactantCount(); i < (reactantCount() + productsCount()); i++) {
      String product = species.get(i);
      int state_index = all.get(product);
      product_product *= Math.pow(state[state_index], orders[i]);
    }
    float[] result = new float[state.length];
    normalized_rate = forward_k*react_product - backward_k*product_product;
    /*Reactant changes*/
    for(int i = 0; i < reactantCount(); i++) {
      String reactant = species.get(i);
      int state_index = all.get(reactant);
      result[state_index] = -normalized_rate * coefficients[i];
    }
    /*Product changes*/
    for(int j = reactantCount(); j < (reactantCount() + productsCount()); j++) {
      String product = species.get(j);
      int state_index = all.get(product);
      result[state_index] = normalized_rate * coefficients[j];
    }
    return result;
  }
  
  //A reaction should know what color to produce at a point given concentrations of products and reactants
  abstract color produceColor(float[] state);
  
  //abstraction
  abstract int reactantCount();
  
  //abstraction
  abstract int productsCount();
}
