/**
 * Represents a single homogenous volume in which a reaction occurs.
 * Cells diffues their contents into other Cells
 */
public class Cell {
  
  float[] concentration_v;
  //Neighbor lists
  Cell up;
  Cell down;
  Cell left;
  Cell right;
  Cell front;
  Cell back;
  //This cell's reaction
  List<Reaction> r_list;
  float[] updated;
  
  //Cells are created with an initial concentration of reactants, products, and a
  //reaction that governs the rate and mechanics of the chemical reaction
  public Cell(float[] initial_c, List<Reaction> r_list) {
    this.r_list = r_list;
    this.concentration_v = initial_c;
    this.updated = new float[initial_c.length];
  }
 

  //sets the 6 neighbors of a cell.
  public void setNeighbors(
    Cell up,
    Cell down,
    Cell left,
    Cell right,
    Cell front,
    Cell back) {
    this.up = up;
    this.down = down;
    this.left = left;
    this.right = right;
    this.front = front;
    this.back = back;
  }
  
  /*Given my position in the world, I need to assign my own*/
  public void setNeighborsProcedural(int grid_dim, int i, int j, int k, Cell[][][] world) {
    setNeighbors(world[i][mod(j-1, grid_dim)][k],
            world[i][mod(j+1, grid_dim)][k],
            world[mod(i-1, grid_dim)][j][k],
            world[mod(i+1, grid_dim)][j][k],
            world[i][j][mod(k-1, grid_dim)],
            world[i][j][mod(k+1, grid_dim)]);
  }
  
  /**
   * Returns the derivative vector for this cell of reactant
   * concentrations for a reaction.
   */
float[] react(Map<String, Integer> all) {
  float[] accum = new float[concentration_v.length];
  for(Reaction r : r_list) {
    accum = add(accum, r.react(concentration_v, all));
  }
  return accum;
}
  
  //A cell's color is based on what the Reaction decides the color is.
  color getColor() {
    color result = color(0,0,0,0);
    for(Reaction r : r_list) {
      result = color_add(result, r.produceColor(concentration_v));
    }
    return result;
  }
  
  /**
   * Calculates the derivative vector due to diffusion
   */
  float[] diffuse(float spacing, float[] diffusive_cons) {
    float[] result = new float[concentration_v.length];
    for(int i = 0; i < concentration_v.length; i++) {
      float dx2 = ((left.concentration_v[i] - this.concentration_v[i])/spacing - (this.concentration_v[i] - right.concentration_v[i])/spacing)/spacing;
      float dy2 = ((up.concentration_v[i] - this.concentration_v[i])/spacing - (this.concentration_v[i] - down.concentration_v[i])/spacing)/spacing;
      float dz2 = ((front.concentration_v[i] - this.concentration_v[i])/spacing - (this.concentration_v[i] - back.concentration_v[i])/spacing)/spacing;
      result[i] = diffusive_cons[i] * ((dx2 + dy2 + dz2));
    }
    return result;
  }
  
}
