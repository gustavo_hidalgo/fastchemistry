public class ReactionBuilder {
 
  
  private int numReactants;
  private int numProducts;
  private float[] reactantOrders;
  private float[] productOrders;
  private float[] reactantCoeffs;
  private float[] productCoeffs;
  private float forwardK;
  private float backwardK;
  private List<String> names;

  
  public ReactionBuilder() {
  }
  
  Reaction build() {
    float[] combinedOrders = new float[numReactants + numProducts];
    System.arraycopy(reactantOrders, 0 , combinedOrders, 0, reactantOrders.length);
    System.arraycopy(productOrders, 0 , combinedOrders, reactantOrders.length, productOrders.length);
    float[] combinedCoeffs = new float[numReactants + numProducts];
    System.arraycopy(reactantCoeffs, 0 , combinedCoeffs, 0, reactantCoeffs.length);
    System.arraycopy(productCoeffs, 0 , combinedCoeffs, reactantCoeffs.length, productCoeffs.length);
    
    return new Reaction(forwardK, backwardK, combinedCoeffs, combinedOrders, names) {
      public color produceColor(float[] state) {
        color accum = color(0,0,0,0);
        int total = reactantCount() + productsCount();
        float sum = 0.0f;
        for(String name : names) {
          accum = color_add(accum, color_mult(globalPalette[participants.get(name)], 10*state[participants.get(name)]));
          sum += state[participants.get(name)];
        }
        return modAlpha(accum , constrain(sum / 2.0, 0, 0.05));
      }
      
      //abstraction
      public int reactantCount() {
        return numReactants;
      }
      
      //abstraction
      public int productsCount() {
        return numProducts;
      }
      
      public String toString() {
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < reactantCount() - 1; i++) {
          sb.append(coefficients[i] + "*" + names.get(i)).append(" + ");
        }
        sb.append(coefficients[reactantCount() - 1] + "*" + names.get(reactantCount() - 1));
        sb.append("<->");
        for(int i = reactantCount(); i < productsCount() + reactantCount() - 1; i++) {
          sb.append(coefficients[i] + "*" + names.get(i)).append(" + ");
        }
        sb.append(coefficients[productsCount() + reactantCount() - 1] + "*" + names.get(productsCount() + reactantCount() - 1));
        return sb.toString();
      }
    };
  }
  
 
  
  ReactionBuilder setNames(List<String> names) {
    if(names.size() != numReactants + numProducts) throw new IllegalArgumentException("Names must match species in reaction"); 
    this.names = names;
    return this;
  }
  
  ReactionBuilder forwardK(float k) {
    this.forwardK = k;
    return this;
  }
  
  ReactionBuilder backwardK(float k) {
    this.backwardK = k;
    return this;
  }
  
  ReactionBuilder reactants(int n) {
    this.numReactants = n;
    return this;
  }
  
  ReactionBuilder products(int n) {
    this.numProducts = n;
    return this;
  }
  
  ReactionBuilder reactantOrders(float[] orders) {
    if(orders.length != numReactants) throw new IllegalArgumentException("Number of orders must match number of reactants");
    this.reactantOrders = orders;
    return this;
  }
  
  ReactionBuilder productOrders(float[] orders) {
    if(orders.length != numProducts) throw new IllegalArgumentException("Number of orders must match number of products");
    this.productOrders = orders;
    return this;
  }
  
  ReactionBuilder reactantCoefficients(float[] orders) {
    if(orders.length != numReactants) throw new IllegalArgumentException("Number of coefficients must match number of reactants");
    this.reactantCoeffs = orders;
    return this;
  }
  
  ReactionBuilder productCoefficients(float[] orders) {
    if(orders.length != numProducts) throw new IllegalArgumentException("Number of coefficients must match number of products");
    this.productCoeffs = orders;
    return this;
  }
  
  
  
  
  
  
  
  
  
}
