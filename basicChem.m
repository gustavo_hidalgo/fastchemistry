%simulates aA + bB <-> cC + dD given 
A = [1]; %initial concentrations
a = 1;
alpha = 1; %A rate constant
B = [1];
b = 10;
beta = 1; %B rate constant
C = [0];
c = 1;
gamma = 1; %C rate constant
D = [0];
d = 1;
epsilon = 1; %d rate constant
kf = 0.1; %forward reaction rate constant
kb = 0.2; %reverse reaction rate constant
dt = 0.1;
for t = 1:dt:100,
    N_prime = ((kf * A(end).^alpha * B(end).^beta) ...
            - (kb * C(end)^gamma * D(end) .^ epsilon));
    A_prime = -N_prime * a;
    B_prime = -N_prime * b;
    C_prime = N_prime * c;
    D_prime = N_prime * d;
    A = [A A(end)+dt*A_prime];
    B = [B B(end)+dt*B_prime];
    C = [C C(end)+dt*C_prime];
    D = [D D(end)+dt*D_prime];
end
hold on
plot(A)
plot(B)
plot(C)
plot(D)
legend('A','B','C','D');
xlabel('time');
ylabel('Concentration');