%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Jacobs Landscape Poster
% LaTeX Template
% Version 1.1 (14/06/14)
%
% Created by:
% Computational Physics and Biophysics Group, Jacobs University
% https://teamwork.jacobs-university.de:8443/confluence/display/CoPandBiG/LaTeX+Poster
% 
% Further modified by:
% Nathaniel Johnston (nathaniel@njohnston.ca)
%
% This template has been downloaded from:
% http://www.LaTeXTemplates.com
%
% License:
% CC BY-NC-SA 3.0 (http://creativecommons.org/licenses/by-nc-sa/3.0/)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%----------------------------------------------------------------------------------------
%	PACKAGES AND OTHER DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------

\documentclass[final]{beamer}

\usepackage[scale=1.24]{beamerposter} % Use the beamerposter package for laying out the poster

\usetheme{confposter} % Use the confposter theme supplied with this template

\setbeamercolor{block title}{fg=ngreen,bg=white} % Colors of the block titles
\setbeamercolor{block body}{fg=black,bg=white} % Colors of the body of blocks
\setbeamercolor{block alerted title}{fg=white,bg=dblue!70} % Colors of the highlighted block titles
\setbeamercolor{block alerted body}{fg=black,bg=dblue!10} % Colors of the body of highlighted blocks
% Many more colors are available for use in beamerthemeconfposter.sty

%-----------------------------------------------------------
% Define the column widths and overall poster size
% To set effective sepwid, onecolwid and twocolwid values, first choose how many columns you want and how much separation you want between columns
% In this template, the separation width chosen is 0.024 of the paper width and a 4-column layout
% onecolwid should therefore be (1-(# of columns+1)*sepwid)/# of columns e.g. (1-(4+1)*0.024)/4 = 0.22
% Set twocolwid to be (2*onecolwid)+sepwid = 0.464
% Set threecolwid to be (3*onecolwid)+2*sepwid = 0.708

\newlength{\sepwid}
\newlength{\onecolwid}
\newlength{\twocolwid}
\newlength{\threecolwid}
\setlength{\paperwidth}{48in}                   % A0 width: 46.8in
\setlength{\paperheight}{36in}                  % A0 height: 33.1in
\setlength{\sepwid}{0.024\paperwidth}           % Separation width (white space) between columns
\setlength{\onecolwid}{0.301\paperwidth}        % Width of one column
\setlength{\twocolwid}{0.626\paperwidth}        % Width of two columns
\setlength{\threecolwid}{0.951\paperwidth}      % Width of three columns
\setlength{\topmargin}{-0.5in}                  % Reduce the top margin size
%-----------------------------------------------------------

\usepackage{graphicx}  % Required for including images
\usepackage{wrapfig}   % Required for figure text wrapping
\usepackage{booktabs}  % Top and bottom rules for tables
\usepackage{caption}
\usepackage{subcaption}
\captionsetup{compatibility=false}
%----------------------------------------------------------------------------------------
%	TITLE SECTION 
%----------------------------------------------------------------------------------------

\title{Real-time chemical diffusion reaction systems} % Poster title

\author{Gustavo Hidalgo, Michael Falk, James Wenk} % Author(s)

\institute{Georgia Institute of Technology} % Institution(s)

%----------------------------------------------------------------------------------------

\begin{document}

\addtobeamertemplate{block end}{}{\vspace*{2ex}} % White space under blocks
\addtobeamertemplate{block alerted end}{}{\vspace*{2ex}} % White space under highlighted (alert) blocks

\setlength{\belowcaptionskip}{2ex} % White space under figures
\setlength\belowdisplayshortskip{2ex} % White space under equations

\begin{frame}[t] % The whole poster is enclosed in one beamer frame

\begin{columns}[t] % The whole poster consists of three major columns, the second of which is split into two columns twice - the [t] option aligns each column's content to the top

\begin{column}{\sepwid}\end{column} % Empty spacer column

\begin{column}{\onecolwid} % The first column

%----------------------------------------------------------------------------------------
%	OBJECTIVES
%----------------------------------------------------------------------------------------

\begin{alertblock}{Objectives}

This simulator aims to simulate a system of chemical reactions in a volume that evolves through time with the following features:
\begin{itemize}
\item Species can diffuse through space
\item Stochiometric coefficients are respected
\item Individual reaction rate constants and partial order contributions
\item Interactivity via introducing species and modifying reaction rates
\end{itemize}

\end{alertblock}

%----------------------------------------------------------------------------------------
%	INTRODUCTION
%----------------------------------------------------------------------------------------

\begin{block}{Conceptual Model}

The mathematical formulation for all reaction-diffusion systems has the form:
\begin{equation}
    \label{eq:r-d}
            \frac{\partial\phi}{\partial t} =
            D(\phi) + R(\phi)
\end{equation}
    


Where $D(\phi_{i})$ represents a diffusive function and $R(\phi_{i})$ represents a reaction function.
In our model, we chose 
\begin{align*}
    D(\phi_i) &= d_i\nabla^2\phi_i &R(\phi_i) &= k\prod_{i=1}^{n}[\phi_i]^{P_i}
\end{align*}
Which are Fick's second law of diffusion and the classical chemical reaction rate model for 
some specie in a universe of n species. We partition space into cubic cells where all 
species are well-mixed. Cells diffuse and react according to the previous equations. In order 
to calculate the Laplacian, we chose the 6 nearest neighbors to a cell
\begin{figure}[h]
    \centering
    \includegraphics[width=0.5\textwidth]{cells.png}
    \caption{Diffusion participants}
\end{figure}

\end{block}

%------------------------------------------------


%----------------------------------------------------------------------------------------

\end{column} % End of the first column

\begin{column}{\sepwid}\end{column} % Empty spacer column

\begin{column}{\onecolwid}

%----------------------------------------------------------------------------------------
%	MATERIALS
%----------------------------------------------------------------------------------------

\begin{block}{Implementation}

The simulator was implemented using Processing, a Java library used for real-time
visualization built on top of OpenGL. Our conceptual model used forward Euler integration
to simulate the differential equations over time.
\begin{wrapfigure}{r}{0.6\textwidth}
    \centering
        \includegraphics[width=0.6\textwidth]{Simulator.png}
    \caption{Simulator architecture}
\end{wrapfigure}

A traditional volume renderer would have been too slow for our goals, therefore
we implemented a rough volume renderer using transparent textures mapped onto 
quads that are drawn back to front (from the view-point of the camera) such that
the opacity values blend and give the appearance of light absorption in space.




The users are able to inject specific species into the reaction volume by moving
a cursor in space. They are also able to select reactions and modify their rate
constants in real time in order to visualize changes to the dynamic equilibrium.

We wanted to recreate the phenomenon in a chemical titration wherein a species would briefly become visible and then invisible due to diffusion and reaction equilibrium shifting. We achieved that goal.

\begin{figure}
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=.6\linewidth]{sim_pic.png}
  \caption{Simulator Example}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=.6\linewidth]{titration.jpg}
  \caption{Real Titration}
\end{subfigure}
\end{figure}

\end{block}

\end{column} % End of the second column

\begin{column}{\sepwid}\end{column} % Empty spacer column

\begin{column}{\onecolwid} % The third column

\begin{block}{Mathematical Section}

Per update there are 2 components: the diffusion and reaction. A general system of $r$ chemical reactions and $s$ species, they can be described as the following 

\begin{equation} % General Reaction System
\tag{General System of Reactions}
\sum_{i=1}^s l_{ij}U_i \rightarrow \sum_{i=1}^s r_{ij}U_i,\quad j = 1..r
\end{equation}

\begin{equation} % Reaction Rate for reaction j
\tag{Reaction Rate}
g_j(u) = k_j \prod_{i=1}^s (u_i)^{l_{ij}}
\end{equation}

\begin{equation} % Change in concentration
\tag{Change In Concentration}
\frac{du_i}{dt} = \sum_{j=1}^r (r_{ij} - l_{ij})g_j(u),\quad i = 1..s
\end{equation}

where $l$ and $r$ reflect the reaction and product coefficients, respectively, of some species, $U_i$ describes the concentrations of the $i$th species in the cell, and $g_j$ describes the rate of the $j$th reaction in the cell. 

\end{block}

%----------------------------------------------------------------------------------------
%	Errors and Future Work
%----------------------------------------------------------------------------------------

\begin{block}{Simulator Limiations}

Our assumptions lead to some significant inaccuracies in the simulator:
\begin{itemize}
\item Significant roundoff errors by using single precision floats instead of double precision
\item Not a true fluid simulation
\item No temperature changes
\item Periodic boundary conditions
\item No phase changes
\item Not a true volume renderer
\item Limited interactivity
\end{itemize}



\end{block}

%----------------------------------------------------------------------------------------
%	Extended Model
%----------------------------------------------------------------------------------------

\begin{block}{Future Work}

The simulation could parallelized using spatial partitioning on a GPU for real time performance.

Implementing the Navier-Stokes equation for fluid dynamics would also improve
the fidelity of the simulation.

Adding more phenomenom would be conceptually as simple as adding more terms to equation (1).

A true ray traced volume renderer would give more accurate visuals at the cost of computational expense.



\end{block}


%----------------------------------------------------------------------------------------

\end{column} % End of the third column

\end{columns} % End of all the columns in the poster

\end{frame} % End of the enclosing frame

\end{document}
